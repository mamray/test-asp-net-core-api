﻿using System;
using Microsoft.EntityFrameworkCore;

namespace SIENN.Services
{
    public partial class SiennServices : IDisposable
    {
        
        protected IUnitOfWork _unitOfWork;

        private SiennServices()
        {
            _unitOfWork = null;
        }

        public SiennServices(DbContext context)
        {
            _unitOfWork = new UnitOfWork(context);
        }

        #region IDisposable
        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    ((IDisposable)_unitOfWork).Dispose();
                }
            }
            this._disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
