﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using SIENN.DbAccess.Entities;

/// <summary>
/// Categories service business logic implementation
/// </summary>
namespace SIENN.Services
{
    public interface ICategoriesService
    {
        bool CategoryExists(int id);
        Category GetCategory(int id);
        IEnumerable<Category> FindCategory(Expression<Func<Category, bool>> predicate);
        IEnumerable<Category> GetAllCategories();
        IEnumerable<Category> GetCategoriesRange(int start, int count);
        IEnumerable<Category> GetCategoriesRange(int start, int count, Expression<Func<Category, bool>> predicate);

        int CategoriesCount();

        void AddCategory(Category entity);
        void UpdateCategory(Category entity);
        void RemoveCategory(Category entity);
    }

    public partial class SiennServices : ICategoriesService
    {
        public void AddCategory(Category entity)
        {
            _unitOfWork.CategoriesRepository.Add(entity);
            _unitOfWork.Save();
        }

        public bool CategoryExists(int id)
        {
            return _unitOfWork.CategoriesRepository.Exists(id);
        }

        public Category GetCategory(int id)
        {
            return _unitOfWork.CategoriesRepository.Get(id);
        }

        public int CategoriesCount()
        {
            return _unitOfWork.CategoriesRepository.Count();
        }

        public IEnumerable<Category> FindCategory(Expression<Func<Category, bool>> predicate)
        {
            return _unitOfWork.CategoriesRepository.Find(predicate);
        }

        public IEnumerable<Category> GetAllCategories()
        {
            return _unitOfWork.CategoriesRepository.GetAll();
        }

        public IEnumerable<Category> GetCategoriesRange(int start, int count)
        {
            return _unitOfWork.CategoriesRepository.GetRange(start, count);
        }

        public IEnumerable<Category> GetCategoriesRange(int start, int count, Expression<Func<Category, bool>> predicate)
        {
            return _unitOfWork.CategoriesRepository.GetRange(start, count, predicate);
        }

        public void RemoveCategory(Category entity)
        {
            _unitOfWork.CategoriesRepository.Remove(entity);
            _unitOfWork.Save();
        }

        public void UpdateCategory(Category entity)
        {
            _unitOfWork.CategoriesRepository.Update(entity);
            _unitOfWork.Save();
        }
    }
}
