﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using SIENN.DbAccess.Entities;

/// <summary>
/// Products service business logic implementation
/// </summary>
namespace SIENN.Services
{
    public interface IProductsService
    {
        bool ProductExists(int id);
        Product GetProduct(int id);
        Product GetProductFull(int id);
        IEnumerable<Product> FindProduct(Expression<Func<Product, bool>> predicate);
        IEnumerable<Product> GetAllProducts();
        IEnumerable<Product> GetProductsRange(int start, int count);
        IEnumerable<Product> GetProductsRange(int start, int count, Expression<Func<Product, bool>> predicate);
        IEnumerable<Product> SearhcProducts(Product filter);

        int ProductsCount();

        void AddProduct(Product entity);
        void UpdateProduct(Product entity);
        void RemoveProduct(Product entity);
    }

    public partial class SiennServices : IProductsService
    {        
        public bool ProductExists(int id)
        {
            return _unitOfWork.ProductsRepository.Exists(id);
        }

        public Product GetProduct(int id)
        {
            var result = _unitOfWork.ProductsRepository.Get(id);            
            return result;
        }

        public Product GetProductFull(int id)
        {
            Product result = null;
            var product = _unitOfWork.ProductsRepository.Get(id, a => a.ProductCategories, a => a.Unit, a => a.Type);
            if(product != null)
            {
                result = new Product
                {
                    Id = product.Id,
                    Code = product.Code,
                    DeliveryDate = product.DeliveryDate,                    
                    Description = product.Description,                    
                    Price = product.Price,                    
                    IsAvailable = product.IsAvailable,
                    CreateDate = product.CreateDate,
                    LastModifiedDate = product.LastModifiedDate,
                    TypeId = product.TypeId,
                    Type = product.Type,
                    Unit = product.Unit,
                    UnitId = product.UnitId
                };

                if(product.ProductCategories != null && product.ProductCategories.Count > 0)
                {
                    result.Categories = new List<Category>();
                    foreach(var item in product.ProductCategories)
                    {
                        var cat = GetCategory(item.CategoryId);
                        if (cat != null)
                            result.Categories.Add(cat);
                    }
                }
            }
            return result;
        }

        public int ProductsCount()
        {
            return _unitOfWork.ProductsRepository.Count();
        }

        public IEnumerable<Product> FindProduct(Expression<Func<Product, bool>> predicate)
        {
            return _unitOfWork.ProductsRepository.Find(predicate);
        }

        public IEnumerable<Product> GetAllProducts()
        {
            return _unitOfWork.ProductsRepository.GetAll();
        }

        public IEnumerable<Product> GetProductsRange(int start, int count)
        {
            return _unitOfWork.ProductsRepository.GetRange(start, count);
        }

        public IEnumerable<Product> GetProductsRange(int start, int count, Expression<Func<Product, bool>> predicate)
        {
            return _unitOfWork.ProductsRepository.GetRange(start, count, predicate);
        }

        public IEnumerable<Product> SearhcProducts(Product filter)
        {
            if(filter == null)
                return _unitOfWork.ProductsRepository.GetAll();
            else
            {
                List<Expression<Func<Product, bool>>> predicates = new List<Expression<Func<Product, bool>>>();
                if (!string.IsNullOrEmpty(filter.Code))
                    predicates.Add(a => a.Code == filter.Code);
                if (!string.IsNullOrEmpty(filter.Description))
                    predicates.Add(a => a.Description == filter.Description);
                if (filter.Id > 0)
                    predicates.Add(a => a.Id == filter.Id);
                if (filter.UnitId > 0)
                    predicates.Add(a => a.UnitId == filter.UnitId);
                if (filter.TypeId > 0)
                    predicates.Add(a => a.TypeId == filter.TypeId);

                return _unitOfWork.ProductsRepository.Find(predicates.ToArray());
            }
        }

        public void AddProduct(Product entity)
        {
            _unitOfWork.ProductsRepository.Add(entity);
            _unitOfWork.Save();
        }        
        public void UpdateProduct(Product entity)
        {
            
            _unitOfWork.ProductsRepository.Update(entity);
            _unitOfWork.Save();
        }
        public void RemoveProduct(Product entity)
        {
            _unitOfWork.ProductsRepository.Remove(entity);
            _unitOfWork.Save();
        }
    }
}
