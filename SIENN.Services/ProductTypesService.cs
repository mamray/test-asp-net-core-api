﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using SIENN.DbAccess.Entities;

/// <summary>
/// Product types service business logic implementation
/// </summary>
namespace SIENN.Services
{
    public interface IProductTypesService
    {
        bool ProductTypeExists(int id);
        ProductType GetProductType(int id);
        IEnumerable<ProductType> FindProductType(Expression<Func<ProductType, bool>> predicate);
        IEnumerable<ProductType> GetAllProductTypes();
        IEnumerable<ProductType> GetProductTypesRange(int start, int count);
        IEnumerable<ProductType> GetProductTypesRange(int start, int count, Expression<Func<ProductType, bool>> predicate);

        int ProductTypesCount();

        void AddProductType(ProductType entity);
        void UpdateProductType(ProductType entity);
        void RemoveProductType(ProductType entity);
    }

    public partial class SiennServices : IProductTypesService
    {
        public void AddProductType(ProductType entity)
        {
            _unitOfWork.ProductTypesRepository.Add(entity);
            _unitOfWork.Save();
        }

        public bool ProductTypeExists(int id)
        {
            return _unitOfWork.ProductTypesRepository.Exists(id);
        }

        public ProductType GetProductType(int id)
        {
            return _unitOfWork.ProductTypesRepository.Get(id);
        }

        public int ProductTypesCount()
        {
            return _unitOfWork.ProductTypesRepository.Count();
        }

        public IEnumerable<ProductType> FindProductType(Expression<Func<ProductType, bool>> predicate)
        {
            return _unitOfWork.ProductTypesRepository.Find(predicate);
        }

        public IEnumerable<ProductType> GetAllProductTypes()
        {
            return _unitOfWork.ProductTypesRepository.GetAll();
        }

        public IEnumerable<ProductType> GetProductTypesRange(int start, int count)
        {
            return _unitOfWork.ProductTypesRepository.GetRange(start, count);
        }

        public IEnumerable<ProductType> GetProductTypesRange(int start, int count, Expression<Func<ProductType, bool>> predicate)
        {
            return _unitOfWork.ProductTypesRepository.GetRange(start, count, predicate);
        }

        public void RemoveProductType(ProductType entity)
        {
            _unitOfWork.ProductTypesRepository.Remove(entity);
            _unitOfWork.Save();
        }

        public void UpdateProductType(ProductType entity)
        {
            _unitOfWork.ProductTypesRepository.Update(entity);
            _unitOfWork.Save();
        }
    }
}
