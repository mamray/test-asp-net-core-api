﻿using System;
using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Repositories;

/// <summary>
/// Unit of work patterm implementation
/// </summary>
namespace SIENN.Services
{
    public interface IUnitOfWork
    {
        ProductTypesRepository ProductTypesRepository { get; }
        CategoriesRepository CategoriesRepository { get; }
        ProductsRepository ProductsRepository { get; }
        UnitsRepository UnitsRepository { get; }
        void Save();
    }
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private DbContext _context;
        private ProductTypesRepository _productTypesRepository;
        private CategoriesRepository _categoriesRepository;
        private ProductsRepository _productsRepository;
        private UnitsRepository _unitsRepository;

        public UnitOfWork(DbContext context)
        {
            _context = context;
        }

        public ProductTypesRepository ProductTypesRepository
        {
            get
            {
                if(_productTypesRepository == null)
                {
                    _productTypesRepository = new ProductTypesRepository(_context);
                }
                return _productTypesRepository;
            }
        }

        public CategoriesRepository CategoriesRepository
        {
            get
            {
                if(_categoriesRepository == null)
                {
                    _categoriesRepository = new CategoriesRepository(_context);
                }
                return _categoriesRepository;
            }
        }

        public ProductsRepository ProductsRepository
        {
            get
            {
                if(_productsRepository == null)
                {
                    _productsRepository = new ProductsRepository(_context);
                }
                return _productsRepository;
            }
        }

        public UnitsRepository UnitsRepository
        {
            get
            {
                if(_unitsRepository == null)
                {
                    _unitsRepository = new UnitsRepository(_context);
                }
                return _unitsRepository;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        #region IDisposable
        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
