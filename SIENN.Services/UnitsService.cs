﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using SIENN.DbAccess.Entities;

/// <summary>
/// Units service business logic implementation
/// </summary>
namespace SIENN.Services
{
    public interface IUnitsService
    {
        bool UnitExists(int id);
        Unit GetUnit(int id);
        IEnumerable<Unit> FindUnit(Expression<Func<Unit, bool>> predicate);
        IEnumerable<Unit> GetAllUnits();
        IEnumerable<Unit> GetUnitsRange(int start, int count);
        IEnumerable<Unit> GetUnitsRange(int start, int count, Expression<Func<Unit, bool>> predicate);

        int UnitsCount();

        void AddUnit(Unit entity);
        void UpdateUnit(Unit entity);
        void RemoveUnit(Unit entity);
    }

    public partial class SiennServices : IUnitsService
    {
        public void AddUnit(Unit entity)
        {
            _unitOfWork.UnitsRepository.Add(entity);
            _unitOfWork.Save();
        }

        public bool UnitExists(int id)
        {
            return _unitOfWork.UnitsRepository.Exists(id);
        }

        public Unit GetUnit(int id)
        {
            return _unitOfWork.UnitsRepository.Get(id);
        }

        public int UnitsCount()
        {
            return _unitOfWork.UnitsRepository.Count();
        }

        public IEnumerable<Unit> FindUnit(Expression<Func<Unit, bool>> predicate)
        {
            return _unitOfWork.UnitsRepository.Find(predicate);
        }

        public IEnumerable<Unit> GetAllUnits()
        {
            return _unitOfWork.UnitsRepository.GetAll();
        }

        public IEnumerable<Unit> GetUnitsRange(int start, int count)
        {
            return _unitOfWork.UnitsRepository.GetRange(start, count);
        }

        public IEnumerable<Unit> GetUnitsRange(int start, int count, Expression<Func<Unit, bool>> predicate)
        {
            return _unitOfWork.UnitsRepository.GetRange(start, count, predicate);
        }

        public void RemoveUnit(Unit entity)
        {
            _unitOfWork.UnitsRepository.Remove(entity);
            _unitOfWork.Save();
        }

        public void UpdateUnit(Unit entity)
        {
            _unitOfWork.UnitsRepository.Update(entity);
            _unitOfWork.Save();
        }
    }
}
