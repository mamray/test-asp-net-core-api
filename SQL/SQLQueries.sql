/* 1. Unavailable products which delivery is expected in current month */
SELECT * 
FROM dbo.Products 
WHERE IsNull(IsAvailable, 0) = 0 
	and Year(DeliveryDate) = Year(CURRENT_TIMESTAMP) 
    AND Month(DeliveryDate) = Month(CURRENT_TIMESTAMP)
    
/* 2. Available products that are assigned to more than one category */
SELECT * 
FROM dbo.Products p1
WHERE IsNull(IsAvailable, 0) = 1 
and exists (select count(*) from dbo.ProductCategory where ProductId = p1.Id having count(*) > 1)

/* 3. Top 3 categories with info about numbers of available, assigned products with its mean price
in category (top 3 should display categories which mean price is the highest)*/
-- Query returns denormalized result set with top 3 categories per filter described and products assosiated
SELECT c1.Code as CategoryCoode, c1.[Description] as CategoryDescription, c3.MeanPrice, c3.NoOfAvailable,
p1.Code as ProductCode, p1.[Description] as ProductDescription, p1.Price as ProductPrice
FROM 
dbo.Categories c1
INNER JOIN dbo.ProductCategory pc1 ON c1.Id = pc1.CategoryId
INNER JOIN dbo.Products p1 ON p1.Id = pc1.ProductId
INNER JOIN 
(
	SELECT TOP 3 c2.Id, 
	SUM(case p2.IsAvailable when 1 then 1 else 0 end) as NoOfAvailable, 
	AVG(p2.Price) MeanPrice
	FROM
	dbo.Categories c2
	INNER JOIN dbo.ProductCategory pc2 ON c2.Id = pc2.CategoryId
	INNER JOIN dbo.Products p2 ON p2.Id = pc2.ProductId
	GROUP By c2.Id
	ORDER BY MeanPrice DESC
) c3 ON c1.Id = c3.Id


