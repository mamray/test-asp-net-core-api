﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

using SIENN.DbAccess;
using SIENN.DbAccess.Entities;
using SIENN.Services;

namespace SIENN.WebApi.Controllers
{
    [Route("api/types")]
    public class TypesController : Controller
    {
        private readonly IProductTypesService _service;

        public TypesController(SiennDb context)
        {
            _service = new SiennServices(context);
        }

        [HttpGet]
        public IEnumerable<ProductType> GetAll()
        {
            return _service.GetAllProductTypes();
        }

        [HttpGet("{id}", Name = "GetType")]
        public IActionResult GetById(int id)
        {
            var item = _service.GetProductType(id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Add([FromBody] ProductType item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _service.AddProductType(item);

            return new ObjectResult(item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] ProductType item)
        {
            if (item == null || item.Id != id)
            {
                return BadRequest();
            }

            if (!_service.ProductTypeExists(id))
            {
                return NotFound();
            }

            _service.UpdateProductType(item);

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = _service.GetProductType(id);
            if (item == null)
            {
                return NotFound();
            }

            _service.RemoveProductType(item);
            return Ok();
        }

        #region IDisposable
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                ((IDisposable)_service).Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
