﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

using SIENN.DbAccess;
using SIENN.DbAccess.Entities;
using SIENN.Services;

namespace SIENN.WebApi.Controllers
{
    [Route("api/units")]
    public class UnitsController : Controller
    {
        private readonly IUnitsService _service;

        public UnitsController(SiennDb context)
        {
            _service = new SiennServices(context);            
        }

        [HttpGet]
        public IEnumerable<Unit> GetAll()
        {
            return _service.GetAllUnits();
        }

        [HttpGet("{id}", Name = "GetUnit")]
        public IActionResult GetById(int id)
        {
            var item = _service.GetUnit(id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Add([FromBody] Unit item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _service.AddUnit(item);

            return new ObjectResult(item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Unit item)
        {            
            if (item == null || item.Id != id)
            {
                return BadRequest();
            }

            if (!_service.UnitExists(id))
            {
                return NotFound();
            }

            _service.UpdateUnit(item);

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = _service.GetUnit(id);
            if (item == null)
            {
                return NotFound();
            }

            _service.RemoveUnit(item);
            return Ok();
        }

        #region IDisposable
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                ((IDisposable)_service).Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
