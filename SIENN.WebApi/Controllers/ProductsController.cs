﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

using SIENN.DbAccess;
using SIENN.DbAccess.Entities;
using SIENN.Services;

namespace SIENN.WebApi.Controllers
{
    [Route("api/products")]
    public class ProductsController : Controller
    {
        private readonly IProductsService _service;

        public ProductsController(SiennDb context)
        {
            _service = new SiennServices(context);
        }

        /// <summary>
        /// API method to return all products
        /// /api/products
        /// </summary>
        /// <returns>list of products</returns>
        [HttpGet]
        public IEnumerable<Product> GetAll()
        {
            return _service.GetAllProducts();
        }

        /// <summary>
        /// API method to return extended information about single product
        /// /api/products/1
        /// </summary>
        /// <param name="id">Id of the product</param>
        /// <returns>product object</returns>
        [HttpGet("{id}", Name = "GetProduct")]
        public IActionResult GetById(int id)
        {
            var item = _service.GetProductFull(id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        /// <summary>
        /// API method that will display only available products (use data pagination)
        /// /api/products/available
        /// /api/products/available?pageNo=1
        /// /api/products/available?pageNo=1&pageSize=10
        /// </summary>
        /// <param name="pageNo">starting page number, 1 is the first page</param>
        /// <param name="pageSize">size of the page to return</param>
        /// <returns></returns>
        [HttpGet]
        [Route("available")]
        public IEnumerable<Product> GetAvailableProducts(int pageNo = 1, int pageSize = 20)
        {
            var fromRow = (pageNo - 1) * pageSize;            
            return _service.GetProductsRange(fromRow, pageSize, p => p.IsAvailable);
        }

        /// <summary>
        /// Do product search by complex filter
        /// </summary>
        /// <param name="filter">product object which fields are used to create filter</param>
        /// <returns>list of products</returns>
        [HttpPost("search")]
        public IEnumerable<Product> SearchProducts([FromBody] Product filter)
        {
            return _service.SearhcProducts(filter);
        }

        /// <summary>
        /// Get simplified information about product
        /// /api/products/info/1
        /// </summary>
        /// <param name="id">id of the product</param>
        /// <returns>product information</returns>
        [HttpGet("info/{id}", Name = "GetProductInfo")]        
        public IActionResult GetProductInformation(int id)
        {
            var item = _service.GetProductFull(id);
            if (item == null)
            {
                return NotFound();
            }

            var result = new
            {
                ProductDescription = item.Description,
                Price = string.Format("{0:0.00} zl", item.Price),
                IsAvailable = item.IsAvailable ? "Available" : "Unavailable",
                DeliveryDate = item.DeliveryDate.ToString("dd.MM.yyyy"),
                CategoriesCount = item.Categories != null ? item.Categories.Count : 0,
                Type = item.Type != null ? $"({item.Type.Code}) {item.Type.Description}" : "",
                Unit = item.Unit != null ? $"({item.Unit.Code}) {item.Unit.Description}" : ""
            };

            return new ObjectResult(result);
        }

        /// <summary>
        /// API method to insert product into database
        /// </summary>
        /// <param name="item">product object</param>
        /// <returns>inserted product object</returns>
        [HttpPost]
        public IActionResult Add([FromBody] Product item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _service.AddProduct(item);
            item = _service.GetProductFull(item.Id);
            return new ObjectResult(item);
        }

        /// <summary>
        /// API method to update product in database
        /// </summary>
        /// <param name="id">Id of the product</param>
        /// <param name="item">product object to use to update</param>
        /// <returns>no content result</returns>
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Product item)
        {
            if (item == null || item.Id != id)
            {
                return BadRequest();
            }

            if (!_service.ProductExists(id))
            {
                return NotFound();
            }

            _service.UpdateProduct(item);

            return Ok();
        }

        /// <summary>
        /// API method to remove product from DB
        /// </summary>
        /// <param name="id">Id of the product to remove</param>
        /// <returns>no content result</returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = _service.GetProduct(id);
            if (item == null)
            {
                return NotFound();
            }

            _service.RemoveProduct(item);
            return Ok();
        }

        #region IDisposable
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                ((IDisposable)_service).Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
