﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

using SIENN.DbAccess;
using SIENN.DbAccess.Entities;
using SIENN.Services;

namespace SIENN.WebApi.Controllers
{
    [Route("api/categories")]
    public class CategoriesController : Controller
    {
        private readonly ICategoriesService _service;

        public CategoriesController(SiennDb context)
        {
            _service = new SiennServices(context);
        }

        [HttpGet]
        public IEnumerable<Category> GetAll()
        {
            return _service.GetAllCategories();
        }

        [HttpGet("{id}", Name = "GetCategory")]
        public IActionResult GetById(int id)
        {
            var item = _service.GetCategory(id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Add([FromBody] Category item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _service.AddCategory(item);

            return new ObjectResult(item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Category item)
        {
            if (item == null || item.Id != id)
            {
                return BadRequest();
            }

            if (!_service.CategoryExists(id))
            {
                return NotFound();
            }

            _service.UpdateCategory(item);

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = _service.GetCategory(id);
            if (item == null)
            {
                return NotFound();
            }

            _service.RemoveCategory(item);
            return Ok();
        }

        #region IDisposable
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                ((IDisposable)_service).Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
