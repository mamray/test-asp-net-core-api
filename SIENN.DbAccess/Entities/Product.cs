﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIENN.DbAccess.Entities
{
    /// <summary>
    /// Product entity
    /// </summary>
    public class Product : IEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public float Price { get; set; }

        public bool IsAvailable { get; set; }

        public DateTime DeliveryDate { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? TypeId { get; set; }
        public ProductType Type { get; set; }

        public int? UnitId { get; set; }
        public Unit Unit { get; set; }

        public ICollection<ProductCategory> ProductCategories { get; set; }

        [NotMapped]
        public List<Category> Categories { get; set; }
    }
}
