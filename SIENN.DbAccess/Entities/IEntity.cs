﻿using System;

namespace SIENN.DbAccess.Entities
{
    /// <summary>
    /// Common interface to describe business entity
    /// </summary>
    public interface IEntity
    {
        int Id { get; set; }
        string Code { get; set; }
        string Description { get; set; }
        DateTime CreateDate { get; set; }
        DateTime? LastModifiedDate { get; set; }        
    }
}
