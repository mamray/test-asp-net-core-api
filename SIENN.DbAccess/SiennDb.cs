﻿using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Entities;

namespace SIENN.DbAccess
{
    public class SiennDb : DbContext
    {
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Unit> Units { get; set; }

        public SiennDb(DbContextOptions opts):base(opts)
        {
        }   

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductCategory>()
                .HasKey(t => new { t.ProductId, t.CategoryId });
            
            modelBuilder.Entity<ProductCategory>()
                .HasOne(pt => pt.Category)
                .WithMany(p => p.ProductCategories)
                .HasForeignKey(pt => pt.CategoryId);

            modelBuilder.Entity<ProductCategory>()
                .HasOne(pt => pt.Product)
                .WithMany(t => t.ProductCategories)
                .HasForeignKey(pt => pt.ProductId);

            base.OnModelCreating(modelBuilder);
        }
    }
}
