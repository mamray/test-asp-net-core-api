﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using SIENN.DbAccess.Entities;

namespace SIENN.DbAccess.Repositories
{
    public interface IGenericRepository<TEntity> where TEntity : class, IEntity
    {
        bool Exists(int id);
        TEntity Get(int id, params Expression<Func<TEntity, object>>[] navigationProperties);

        IEnumerable<TEntity> Find(params Expression<Func<TEntity, bool>>[] predicate);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetRange(int start, int count);
        IEnumerable<TEntity> GetRange(int start, int count, Expression<Func<TEntity, bool>> predicate);

        int Count();

        void Add(TEntity entity);
        void Update(TEntity entity);
        void Remove(TEntity entity);
    }
}
