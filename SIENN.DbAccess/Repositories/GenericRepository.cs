﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Microsoft.EntityFrameworkCore;

using SIENN.DbAccess.Entities;

namespace SIENN.DbAccess.Repositories
{
    public abstract class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class, IEntity
    {
        protected GenericRepository(DbContext context)
        {
            _context = context;
            _entities = context.Set<TEntity>();
        }

        public bool Exists(int id)
        {
            return _entities.Any(o => o.Id == id);
        }

        public virtual TEntity Get(int id, params Expression<Func<TEntity, object>>[] navigationProperties)
        {
            IQueryable<TEntity> dbQuery = _entities;
            
            foreach (Expression<Func<TEntity, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include<TEntity, object>(navigationProperty);
            dbQuery = dbQuery.AsNoTracking();
            return dbQuery.FirstOrDefault(a => a.Id == id);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return _entities.ToList();
        }

        public virtual IEnumerable<TEntity> GetRange(int start, int count)
        {
            return _entities.Skip(start).Take(count).ToList();
        }

        public virtual IEnumerable<TEntity> GetRange(int start, int count, Expression<Func<TEntity, bool>> predicate)
        {
            return _entities.Where(predicate).Skip(start).Take(count).ToList();
        }

        public virtual IEnumerable<TEntity> Find(params Expression<Func<TEntity, bool>>[] predicate)
        {
            IQueryable<TEntity> dbQuery = _entities;
            foreach(Expression<Func<TEntity, bool>> expr in predicate)
            {
                dbQuery = dbQuery.Where(expr);
            }
            return dbQuery.ToList<TEntity>();
        }

        public virtual int Count()
        {
            return _entities.Count();
        }

        public virtual void Add(TEntity entity)
        {
            entity.CreateDate = DateTime.Now;
            _entities.Add(entity);
        }        

        public virtual void Update(TEntity entity)
        {
            entity.LastModifiedDate = DateTime.Now;
            AttachIfDetached(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Remove(TEntity entity)
        {
            _entities.Remove(entity);
        }

        protected void AttachIfDetached(TEntity entity)
        {
            if (_context.Entry<TEntity>(entity).State == EntityState.Detached)
            {
                _entities.Attach(entity);
            }
        }

        private DbSet<TEntity> _entities;
        private DbContext _context;
    }
}
