﻿using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Entities;

namespace SIENN.DbAccess.Repositories
{
    public class UnitsRepository : GenericRepository<Unit>
    {
        public UnitsRepository(DbContext context) : base(context)
        {
        }
    }
}
