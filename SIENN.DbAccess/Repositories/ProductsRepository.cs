﻿using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Entities;

namespace SIENN.DbAccess.Repositories
{
    public class ProductsRepository : GenericRepository<Product>
    {
        public ProductsRepository(DbContext context) : base(context)
        {
        }
    }
}
