﻿using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Entities;

namespace SIENN.DbAccess.Repositories
{
    public class ProductTypesRepository : GenericRepository<ProductType>
    {
        public ProductTypesRepository(DbContext context) : base(context)
        {
        }
    }
}
